import React from 'react';

import {Card, Button} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Order({orderProp}){


	return (

			/*<Card className="text-center crd mx-auto mb-4">
 		 	<Card.Header>Order Slip</Card.Header>
  			<Card.Body className="crdBody">
    			<Card.Title>{orderProp._id}</Card.Title>
     			<Card.Subtitle className="mt-3">Details:</Card.Subtitle>
     			<Card.Text>{orderProp.products}</Card.Text>
				<Card.Subtitle>Total Amount:</Card.Subtitle>
		 		<Card.Text>PHP {orderProp.totalAmount}</Card.Text>
		 		
  			</Card.Body>
  			<Card.Footer className="text-muted">
  				{orderProp.datePurchased}
  			</Card.Footer>
		</Card>
  */
      <Card className="text-center crd mx-auto mb-4">
      <Card.Header className="bg-danger text-white">Order Slip</Card.Header>
        <Card.Body className="crdBody">
         
          
          <Card.Title>{orderProp.products.map((item)=>{

            return <p>{item.name}</p>
          })}</Card.Title>
        <Card.Subtitle>Total Amount:</Card.Subtitle>
        <Card.Text>PHP {orderProp.totalAmount}</Card.Text>
        
        </Card.Body>
        <Card.Footer className="text-muted">
          {orderProp.datePurchased}
        </Card.Footer>
    </Card>
	)
}