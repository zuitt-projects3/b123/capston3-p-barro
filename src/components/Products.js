import React from 'react';

import {Card, Button, Row, Col} from 'react-bootstrap';

import {Link} from 'react-router-dom';

export default function Product({productProp}){



	return (

		
      <Col className="text-center mx-auto my-5" style={{ width: '50rem' }}>
        <Card className="crdBody">
        <Card.Header className="bg-info text-white"><strong>Game</strong></Card.Header>
        <Card.Body>
          <Card.Title><strong>{productProp.name}</strong></Card.Title>
          
          
       
          <Card.Text className="txtGenre">{productProp.genre}</Card.Text>
        
        
        
        </Card.Body>
        <Card.Footer className="text-muted">
          <Link className="btn btn-danger" to={`/products/${productProp._id}`}>Game Details</Link>
        </Card.Footer>
    </Card>

      </Col>  
     
    
		
	)
}